import { Component, OnInit } from '@angular/core';
import { Category } from '../models/category.model';
import { CategoriesService } from '../providers/categories.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ServicesService } from '../providers/services.service';
import { Service } from '../models/service.model';



@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})

export class SearchComponent implements OnInit {
  categories: Array<Category> = [];
  matchingServices: Array<Service> = [];
  servicesService: any;


  constructor(private categoriesService: CategoriesService) { }
  ngOnInit() {
    // call getToDos() method in ToDoService
    this.categoriesService.getCategories().subscribe(data => {
      this.categories = data;
    });
  }

onSelectCategory(event: any): void {
  const selectedCategory = event.target.value;

  // if they pick the select one option... show nothing
  if (selectedCategory == "") {
    this.matchingServices = [];
  }
  else {
    // otherwise... find the matching sayings and show
    this.servicesService.getServices().subscribe((data: any[])=>{
      //console.log(data);
      // TODO
      this.matchingServices = data.filter(d => d.ServiceID.startsWith(selectedCategory.substring(0,4)) );  
   })
    // this.matchingServices =
    //   this.servicesService.getServices(
    //     selectedCategory);
  }
}
}

