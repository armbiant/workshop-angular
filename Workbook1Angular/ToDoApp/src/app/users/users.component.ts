import { Component, OnInit } from '@angular/core';
import { User } from '../models/user.model';
import { UserServiceService } from '../providers/user-service.service';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: Array<User> = [];
  constructor(private UserServiceService: UserServiceService) { }
  ngOnInit() {
    // call getToDos() method in ToDoService
    this.UserServiceService.getUsers().subscribe(data => {
      this.users = data;
    });
  }
}