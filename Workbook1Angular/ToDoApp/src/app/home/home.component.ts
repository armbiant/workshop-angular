import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  intro = "Lorem ipsum dolor sit amet. Ea sapiente consequatur qui ullam perspiciatis est aspernatur ipsa qui soluta quas et error pariatur aut dicta veniam? Vel quod odio sit dolore voluptatem eos tempore consequatur. Qui nesciunt maiores sed eius laborum qui fuga omnis non earum autem non velit modi.";

}
